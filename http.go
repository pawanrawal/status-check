package main

import (
	"flag"
	"fmt"
	"html/template"
	"io"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/pawanrawal/status-check/database"
	"gitlab.com/pawanrawal/status-check/services"
)

var (
	port   = flag.Int("p", 1323, "Port on which to start http server.")
	public = flag.String("public", "public", "Relative path to the public folder.")
)

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {
	flag.Parse()
	defer database.DB.Close()
	e := echo.New()
	e.Use(middleware.Logger())

	tpl, err := template.New("ui").ParseGlob(*public + "/views/*.html")
	if err != nil {
		// Show error message that we couldn't find public folder.
		e.GET("/", func(c echo.Context) error {
			return echo.NewHTTPError(http.StatusBadRequest,
				fmt.Sprintf("Couldn't find any html files in the public folder: [%s]. "+
					"Did you specify the correct path using the -public flag?", *public))
		})
	} else {
		// Render the template with services.
		t := &Template{
			templates: tpl,
		}
		e.Renderer = t

		e.GET("/", func(c echo.Context) error {
			s, err := services.All()
			if err != nil {
				c.String(http.StatusInternalServerError, err.Error())
				return nil
			}
			return c.Render(http.StatusOK, "services", s)
		})

	}

	e.POST("/services", services.Add)
	e.GET("/services", services.Index)
	e.GET("/services/:id", services.Get)
	e.POST("/ping", services.Ping)

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", *port)))
}
