package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/boltdb/bolt"
)

var (
	name           = flag.String("name", "m1", "Name of the service.")
	statusCheckUrl = flag.String("status_check", "http://localhost:1323", "Url of status check service.")
	pingInterval   = flag.Duration("ping_interval", 5*time.Second,
		"Interval at which to ping status check service.")
)

type Service struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func addService() int {
	input := []byte(`{"name":"` + *name + `"}`)
	addServiceUrl := fmt.Sprintf("%s/services", *statusCheckUrl)

	req, err := http.NewRequest("POST", addServiceUrl, bytes.NewBuffer(input))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusCreated {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalf("Error while reading body: %v", err)
		}
		log.Fatalf("Expected status code 201. Got: [%v] with message: %v", http.StatusCreated,
			string(body))
	}

	var s Service
	err = json.NewDecoder(resp.Body).Decode(&s)
	if err != nil {
		log.Fatalf("Error while decoding response as JSON: %v", err)
	}

	if s.Id == 0 {
		log.Fatalf("Expected Id to be > 0.")
	}
	return s.Id
}

func ping(id int) {
	t := time.NewTicker(*pingInterval)
	pingUrl := fmt.Sprintf("%s/ping?service_id=%d", *statusCheckUrl, id)
	for range t.C {
		// The microservice can also do other checks like checking DB is healthy etc. before making
		// a ping.
		req, err := http.NewRequest("POST", pingUrl, nil)
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			fmt.Printf("Error while making ping request: %+v\n", err)
			continue
		}
		if resp.StatusCode != http.StatusOK {
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Printf("Error while reading body from ping: %v\n", err)
			}
			fmt.Printf("Expected status code 200. Got: [%v] with message: %v\n", http.StatusCreated,
				string(body))
		}

	}
}

func fetchId(db *bolt.DB) int {
	var id int
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("MyBucket"))
		v := b.Get([]byte("id"))
		if v == nil {
			return nil
		}
		var err error
		id, err = strconv.Atoi(string(v))
		return err
	})
	if err != nil {
		log.Fatalf("Error while fetching id from bolt: %v", err)
	}
	return id
}

func storeId(db *bolt.DB, id int) {
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("MyBucket"))
		byteId := []byte(strconv.Itoa(id))
		err := b.Put([]byte("id"), byteId)
		return err
	})
	if err != nil {
		log.Fatalf("Error while updating id in bolt: %v", err)
	}

}

func main() {
	flag.Parse()
	// We persist the id that we get from Status Check service in boltdb. This is so that on restart
	// of this microservice, we can start ping directly.
	db, err := bolt.Open(fmt.Sprintf("%s.db", *name), 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("MyBucket"))
		return err
	})
	if err != nil {
		log.Fatalf("Error while trying to create bucket: %v", err)
	}

	id := fetchId(db)
	if id == 0 {
		id = addService()
		storeId(db, id)
		fmt.Println("Microservice added with Status Check service.")
	} else {
		fmt.Printf("Id found for microservice: [%d]\n", id)
	}

	go ping(id)
	c := make(chan struct{})
	<-c
}
