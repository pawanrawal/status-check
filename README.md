# Status Check

Status Check is a solution for monitoring all your microservices. Its fast, easy to setup and
simple to use.

## Getting Started

Once you have [Go](https://golang.org/doc/install) installed, run the command below to install
Status Check.

```
go get -u -v gitlab.com/pawanrawal/status-check/...
```

Once installed, you can start the binary.

```
status-check
```

Note - The `status-check` binary should be run from the root of the repo for the UI to be available as it
needs access to the public folder. The path to the UI can also be passed using the `--public` flag
like shown below.
```
status-check --public $GOPATH/src/gitlab.com/pawanrawal/status-check/public
```

An HTTP server is started on port `1323` and listens for requests. It has the following
endpoints.

1. POST `/services` - To register a new service. Expects a unique `name` as part of JSON body. If
   the service was registered, an id is returned which must be used to make regular `/ping` calls.
2. POST `/ping?service_id=<id>` - Each registered service must ping routinely to indicate that its
   healthy.
2. GET `/services` - To get all registered services, `last_ping` and `status` are returned for each
   service.
3. GET `/services/:id` - To get a particular service using its id. This can be used to update the
   status of a service on your homepage.

A user interface is available at [`http://localhost:1323`](http://localhost:1323). It shows all
the registered services and their status.

Optional environment variable called `FAILURE_THRESHOLD` can also be set. Default value for the
variable is 30s. So if a registered service doesn't ping the `status` server for 30 seconds it is
marked unhealthy until it pings again.

```
# To set a failure threshold of 60s
FAILURE_THRESHOLD=60s status-check
```

## Examples

### Docker Compose

From the root of the repo, run

```
docker-compose up -d
```

This should start the `status-check` and three other microservices called `m1`, `m2` and `m3`.
You can verify that all containers started successfully using `docker ps`.

Sample output:
```
CONTAINER ID        IMAGE                            COMMAND                  CREATED             STATUS              PORTS                    NAMES
c5edcb065324        pawanrawal/status-check:latest   "status-check -pub..."   5 minutes ago       Up 4 minutes        0.0.0.0:1323->1323/tcp   status-check
94ac47216c1f        pawanrawal/status-check:latest   "microservice -nam..."   5 minutes ago       Up 3 minutes                                 m2
21efce1da52b        pawanrawal/status-check:latest   "microservice -nam..."   5 minutes ago       Up 4 minutes                                 m3
41ab16e44a56        pawanrawal/status-check:latest   "microservice -nam..."   5 minutes ago       Up 4 minutes                                 m1
```

Then goto [`http://localhost:1323`](http://localhost:1323) and check the status of all the services.

Try killing one or all of the services like this.
```
docker stop m2
```

Now the UI should show service m2 as unhealthy, restart it again using
```
docker restart m2
```
and it should be healthy again.

To bring down all the services run
```
docker-compose down
```

### Directly on host

A sample microservice implementation is shown as part of the `microservice` package. We'll try to
start the `status-check` service and three microservices and check their status in the UI.

Run the following commands in different terminal windows.

Note - The `status-check` binary should be run from the root of the repo for the UI to be available as it
needs access to the public folder or the path can be provided using the `-public` flag.

```
FAILURE_THRESHOLD=10s status-check

microservice -name m1
microservice -name m2
microservice -name m3
```

Then goto [`http://localhost:1323`](http://localhost:1323) and check the status of all the services.

Try to kill microservice with name `m1`, wait for 10 seconds and refresh the status page. It should
be unhealthy now. Now try to bring it up again and see it turn healthy.

## TODO

* Add an API to remove a microservice.
* Add an API to update the name of a microservice.
* Bundle in assets with the binary.

## Tests

Run the following command to run the tests.

```
go test -v gitlab.com/pawanrawal/status-check/...
```
