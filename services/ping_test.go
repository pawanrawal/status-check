package services

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func TestPing(t *testing.T) {
	dropData()
	serviceJSON := `{"name": "Jon Snow"}`
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/services", strings.NewReader(serviceJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	assert.NoError(t, Add(c))
	s := Service{}
	assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &s))

	q := make(url.Values)
	q.Set("service_id", fmt.Sprintf("%d", s.Id))
	req = httptest.NewRequest(echo.GET, "/services?"+q.Encode(), nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)

	assert.NoError(t, Ping(c))
	assert.Equal(t, http.StatusOK, rec.Code)

	s1 := Service{}
	assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &s1))
	assert.Equal(t, s.Id, s1.Id)
	assert.Equal(t, HEALTHY, s1.Status)
}
