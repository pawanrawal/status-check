package services

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/labstack/echo"
	sqlite "github.com/mattn/go-sqlite3"
	"gitlab.com/pawanrawal/status-check/database"
)

var (
	// If we don't receive a ping for duration > FAILURE_THRESHOLD we consider a service as
	// unhealthy.
	FAILURE_THRESHOLD time.Duration
)

func init() {
	var err error
	ft := os.Getenv("FAILURE_THRESHOLD")
	if len(ft) == 0 {
		FAILURE_THRESHOLD = 30 * time.Second
		return
	}

	FAILURE_THRESHOLD, err = time.ParseDuration(ft)
	if err != nil {
		log.Fatal("Couldn't parse FAILURE_THRESHOLD as time.Duration")
	}
}

const (
	HEALTHY   = "HEALTHY"
	UNHEALTHY = "UNHEALTHY"
)

type Service struct {
	Id int64 `json:"id" db:"id"`
	// Name of a service. This should be unique across all services.
	Name string `json:"name" db:"name"`
	// LastPing is the time at which we received the last ping from a service.
	LastPing string `json:"last_ping" db:"last_ping"`
	// Status can either be HEALTHY or UNHEALTHY. Its HEALTHY if LastPing was received with
	// FAILURE_THRESHOLD seconds.
	Status string `json:"status"`
}

// updatePing updates LastPing on a Service object. The LastPing is converted to time.UnixDate
// format.
func (s *Service) updatePing() {
	s.LastPing = time.Now().Format(time.UnixDate)
}

// isHealthy checks if a service is healthy i.e. if the lastPing is within the failure threshold.
func (s *Service) isHealthy() (bool, error) {
	lastPing, err := time.Parse(time.UnixDate, s.LastPing)
	if err != nil {
		return false, err
	}

	return time.Now().Sub(lastPing) < FAILURE_THRESHOLD, nil
}

// Add adds a service to the database. It expects the a JSON object with the name of the service to
// add. It returns the created service along with the id which should be used to do `/ping`
// requests.
func Add(c echo.Context) error {
	s := new(Service)
	if err := c.Bind(s); err != nil {
		return err
	}

	if len(s.Name) == 0 {
		return echo.NewHTTPError(http.StatusBadRequest, errors.New("name cannot be empty."))
	}

	s.updatePing()
	s.Status = HEALTHY

	r, err := database.DB.NamedExec("INSERT INTO service (name, last_ping) VALUES (:name, :last_ping)", &s)
	if err != nil {
		if err.(sqlite.Error).Code == sqlite.ErrConstraint {
			return echo.NewHTTPError(http.StatusBadRequest,
				fmt.Sprintf("name should be unique. Already have a record with name: [%s]", s.Name))
		}
		return err
	}
	id, err := r.LastInsertId()
	if err != nil {
		return err
	}
	s.Id = id

	return c.JSON(http.StatusCreated, s)
}

// All queries the database and returns all the registered services.
func All() ([]Service, error) {
	services := []Service{}

	err := database.DB.Select(&services, "SELECT * FROM service")
	if err != nil {
		return services, err
	}

	for idx, service := range services {
		ok, err := service.isHealthy()
		if err != nil {
			return services, err
		}

		s := &services[idx]
		if ok {
			s.Status = HEALTHY
		} else {
			s.Status = UNHEALTHY
		}
	}
	return services, nil

}

// Index returns all services in a JSON object.
func Index(c echo.Context) error {
	services, err := All()
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, services)
}

// Get returns a service. It accepts id of the service as a parameter and returns the matched
// service.
func Get(c echo.Context) error {
	s := Service{}
	id := c.Param("id")
	err := database.DB.Get(&s, "SELECT * FROM service WHERE id=$1", id)
	if err != nil {
		if err == sql.ErrNoRows {
			return echo.NewHTTPError(http.StatusBadRequest,
				fmt.Sprintf("No record found with id: [%s].", id))
		}
		return err
	}

	ok, err := s.isHealthy()
	if err != nil {
		return err
	}

	if ok {
		s.Status = HEALTHY
	} else {
		s.Status = UNHEALTHY
	}

	return c.JSON(http.StatusOK, s)
}
