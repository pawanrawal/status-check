package services

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pawanrawal/status-check/database"
)

func dropData() {
	database.DB.Close()
	var err error
	database.DB, err = sqlx.Connect("sqlite3", ":memory:")
	if err != nil {
		log.Fatal(err)
	}

	database.DB.MustExec(database.Schema)

}

func TestCreateServiceEmptyNameError(t *testing.T) {
	dropData()
	serviceJSON := `{"name": ""}`
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/services", strings.NewReader(serviceJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	err := Add(c)
	assert.Error(t, err)
	assert.Equal(t, `code=400, message=name cannot be empty.`, err.Error())
}

func TestCreateService(t *testing.T) {
	dropData()
	serviceJSON := `{"name": "Jon Snow"}`
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/services", strings.NewReader(serviceJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	assert.NoError(t, Add(c))
	assert.Equal(t, http.StatusCreated, rec.Code)

	s := Service{}
	assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &s))
	assert.NotZero(t, s.Id, 0)
	assert.NotZero(t, s.LastPing, 0)
	assert.Equal(t, HEALTHY, s.Status)
}

func TestGetService(t *testing.T) {
	dropData()
	serviceJSON := `{"name": "Jon Snow"}`
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/services", strings.NewReader(serviceJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	assert.NoError(t, Add(c))
	s := Service{}
	assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &s))

	req = httptest.NewRequest(echo.GET, "/", nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	c.SetPath("/services/:id")
	c.SetParamNames("id")
	c.SetParamValues(fmt.Sprintf("%d", s.Id))

	assert.NoError(t, Get(c))
	assert.Equal(t, http.StatusOK, rec.Code)

	s1 := Service{}
	assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &s1))
	assert.Equal(t, s.Id, s1.Id)
	assert.Equal(t, s.LastPing, s1.LastPing)
	assert.Equal(t, s.Status, s1.Status)
}

func TestGetService_UnhealthyError(t *testing.T) {
	dropData()
	serviceJSON := `{"name": "Jon Snow"}`
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/services", strings.NewReader(serviceJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	assert.NoError(t, Add(c))
	s := Service{}
	assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &s))
	FAILURE_THRESHOLD = 10 * time.Millisecond
	time.Sleep(20 * time.Millisecond)

	req = httptest.NewRequest(echo.GET, "/", nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	c.SetPath("/services/:id")
	c.SetParamNames("id")
	c.SetParamValues(fmt.Sprintf("%d", s.Id))

	assert.NoError(t, Get(c))
	assert.Equal(t, http.StatusOK, rec.Code)

	s1 := Service{}
	assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &s1))
	assert.Equal(t, s.Id, s1.Id)
	assert.Equal(t, UNHEALTHY, s1.Status)
}

func TestIndex(t *testing.T) {
	dropData()
	serviceJSON := `{"name": "Jon Snow"}`
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/services", strings.NewReader(serviceJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	assert.NoError(t, Add(c))

	serviceJSON = `{"name": "Banking"}`
	req = httptest.NewRequest(echo.POST, "/services", strings.NewReader(serviceJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	assert.NoError(t, Add(c))

	req = httptest.NewRequest(echo.GET, "/services", nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)

	assert.NoError(t, Index(c))
	assert.Equal(t, http.StatusOK, rec.Code)
	s := []Service{}
	assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &s))
	assert.Equal(t, 2, len(s))
}
