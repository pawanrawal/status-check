package services

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/pawanrawal/status-check/database"
)

// Ping would be called by a service to indicate that its healthy. service_id is accepted as a query
// parameter. LastPing for a service is updated when ping is called.
func Ping(c echo.Context) error {
	s := Service{}
	id := c.QueryParam("service_id")
	if len(id) == 0 {
		return echo.NewHTTPError(http.StatusBadRequest,
			"service_id is a mandatory query parameter.")
	}

	err := database.DB.Get(&s, "SELECT rowid, * FROM service WHERE rowid=$1", id)
	if err != nil {
		if err == sql.ErrNoRows {
			return echo.NewHTTPError(http.StatusBadRequest,
				fmt.Sprintf("No record found with id: [%s].", id))
		}
		return err
	}

	s.updatePing()

	statement := `
	UPDATE service
	SET last_ping = :last_ping
	WHERE id = :id;`
	r, err := database.DB.NamedExec(statement, map[string]interface{}{
		"id":        id,
		"last_ping": s.LastPing,
	})
	if err != nil {
		return err
	}
	count, err := r.RowsAffected()
	if err != nil {
		return err
	}
	if count != 1 {
		return echo.NewHTTPError(http.StatusInternalServerError,
			fmt.Sprintf("Couldn't update the record with id: [%d]", s.Id))
	}

	s.Status = HEALTHY
	return c.JSON(http.StatusOK, s)
}
