package database

import (
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var (
	DB *sqlx.DB
)

var Schema = `
CREATE TABLE IF NOT EXISTS service (
	id integer primary key autoincrement,
	name text unique,
	last_ping text
)`

func init() {
	var err error
	DB, err = sqlx.Connect("sqlite3", "status.db")
	if err != nil {
		log.Fatal(err)
	}

	DB.MustExec(Schema)
}
